export interface RestResponseInterface {
    total: number;
    limit: number;
    skip: number;
    data: Array<any>;
}