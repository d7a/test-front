import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  userForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {
    this.userForm = this.formBuilder.group({
      '_id': [],
      'name': ['', Validators.required],
      'surname': ['', Validators.required],
      'email': ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  createUser() {
    if ( this.userForm.valid ) {
      const userData = this.userForm.value;
      this.userService.createUser(userData);
      this.router.navigateByUrl('/');
    }
  }

}
