import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

import {UserInterface} from 'src/app/interfaces/user.interface';

import {ApiUserService} from 'src/app/services/api/api.user.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'name', 'surname', 'email', 'actions'];
  dataSource: MatTableDataSource<UserInterface>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  userList: Array<UserInterface>;

  constructor(
    private userService: UserService,
    private router: Router
  ) {

    this.userService.getUserList();

    const userList = this.userService.userList;
    userList.subscribe((data) => {
      this.userList = data;
      this.dataSource = new MatTableDataSource(this.userList);
    });

    this.dataSource = new MatTableDataSource(this.userList);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createUserPage() {
    this.router.navigateByUrl('/create-user');
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  deleteUser( userId ) {
    this.userService.deleteUser( userId );
  }

}
