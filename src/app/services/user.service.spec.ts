import { TestBed } from '@angular/core/testing';
import { UserService } from './user.service';
import { ApiUserService } from 'src/app/services/api/api.user.service';
import { UserInterface } from 'src/app/interfaces/user.interface';
import { RestResponseInterface } from 'src/app/interfaces/rest-response.interface';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers:[ApiUserService]
  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it('getting list of users', () => {
    let restResponse: RestResponseInterface;
    const apiService: ApiUserService = TestBed.get(ApiUserService);
    const service: UserService = TestBed.get(UserService);

    const apiSpy = spyOn(apiService, 'find').and.callFake(function() {
      return Promise.resolve(restResponse);
    });
    service.getUserList();
    expect(apiSpy).toHaveBeenCalled();
  });

  it('create new user', () => {
    let userData: UserInterface;
    const apiService: ApiUserService = TestBed.get(ApiUserService);
    const service: UserService = TestBed.get(UserService);

    const apiSpy = spyOn(apiService, 'create').and.callFake(function() {
      return Promise.resolve(userData);
    });
    service.createUser(userData);
    expect(apiSpy).toHaveBeenCalled();
  });

  it('delete user', () => {
    let userData: UserInterface;
    let fakeId: string;
    const apiService: ApiUserService = TestBed.get(ApiUserService);
    const service: UserService = TestBed.get(UserService);

    const apiSpy = spyOn(apiService, 'remove').and.callFake(function() {
      return Promise.resolve(userData);
    });
    service.deleteUser(fakeId);
    expect(apiSpy).toHaveBeenCalled();
  });
});
