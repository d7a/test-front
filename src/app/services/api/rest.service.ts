import { Injectable } from '@angular/core';
import feathers from '@feathersjs/feathers';
import rest from '@feathersjs/rest-client';


@Injectable({
  providedIn: 'root'
})
export class RestService {

  private feathersEnterpoint = feathers();
  private restClient = rest('http://localhost:3030');

  constructor() {
    this.feathersEnterpoint.configure(this.restClient.fetch(window.fetch));
  }

  getFeathersService( serviceName: string ) {
    return this.feathersEnterpoint.service(serviceName);
  }
}
