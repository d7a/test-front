import { Injectable } from '@angular/core';
import { RestService } from 'src/app/services/api/rest.service';
import { RestResponseInterface } from 'src/app/interfaces/rest-response.interface';

@Injectable({
  providedIn: 'root'
})

export class ApiUserService {

  private restUserService;

  constructor( private restService: RestService ) {
    this.restUserService = this.restService.getFeathersService('user');
  }

  public get(userId = null, params = {}) {
    return this.restUserService.get(userId, params).then((response) => {
      return response;
    }).catch((error) => {
      console.log(error);
    });
  }

  public find(params = {}) {
    return this.restUserService.find(params).then((response) => {
      return response;
    }).catch((error) => {
      console.log(error);
    });
  }

  public create(data = null, params = {}) {
    return this.restUserService.create(data, params).then((response) => {
      return response;
    }).catch((error) => {
      console.log(error);
    });
  }

  public update(userId = null, data = {}, params = {}) {
    return this.restUserService.update(userId, data, params).then((response) => {
      return response;
    }).catch((error) => {
      console.log(error);
    });
  }

  public patch(userId = null, data = {}, params = {}) {
    return this.restUserService.patch(userId, data, params).then((response) => {
      return response;
    }).catch((error) => {
      console.log(error);
    });
  }

  public remove(userId = null, params = {}) {
    return this.restUserService.remove(userId, params).then((response) => {
      return response;
    }).catch((error) => {
      console.log(error);
    });
  }
}
