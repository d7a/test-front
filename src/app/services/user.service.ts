import { Injectable } from '@angular/core';
import { ApiUserService } from 'src/app/services/api/api.user.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _userList: BehaviorSubject<any> = new BehaviorSubject(false);
  public readonly userList: Observable<any> = this._userList.asObservable();

  constructor(private apiUserService: ApiUserService) {
    /*
    this.apiUserService.on('created'), (data) => {
      this.getUserList();
    }
    */
  }

  getUserList(params = {}) {
    this.apiUserService.find(params).then((response) => {
      this._userList.next(response.data);
    }).catch((e) => {
      console.log(e);
    })
  }

  createUser(data) {
    this.apiUserService.create(data).then((response) => {
      this.getUserList();
    }).catch((e) => {
      console.log(e);
    });
  }

  deleteUser(userid) {
    this.apiUserService.remove(userid).then((response) => {
      this.getUserList();
    }).catch((e) => {
      console.log(e);
    });
  }
}
